﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PKP_Tehnicen_dolg_app
{
    /// <summary>
    /// Interaction logic for AddDolg.xaml
    /// </summary>
    public partial class AddDolg : Window
    {
        public string idProjekta { get; set; }

        public AddDolg(string idprojekta)
        {
            InitializeComponent();
            this.idProjekta = idprojekta;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var item = ComBoxKriticnost.SelectedItem;
            var dolg = new Dictionary<string, string>();
            dolg.Add("naziv", NazivBox.Text.ToString());
            dolg.Add("vzrok", "");
            dolg.Add("tip", "");
            dolg.Add("podtip", "");
            dolg.Add("opis", new TextRange(OpisBox.Document.ContentStart, OpisBox.Document.ContentEnd).Text.ToString());
            dolg.Add("obseg", TxtTime.Text.ToString());
            dolg.Add("ocena", "");
            dolg.Add("kriticnost", ((System.Windows.Controls.ComboBoxItem)item).Content.ToString());
            dolg.Add("status", "");
            dolg.Add("datum", ""); //EndDatePicker.Text.ToString()
            dolg.Add("id_projekt", idProjekta);
            dolg.Add("clan", ConfigurationManager.AppSettings.Get("email"));
            dolg.Add("clan1", "");
            dolg.Add("clan2", "");
            dolg.Add("clan3", "");
            dolg.Add("clan4", "");
            dolg.Add("clan5", "");
            dolg.Add("clan6", "");
            dolg.Add("clan7", "");
            dolg.Add("clan8", "");
            dolg.Add("clan9", "");

            var output = Newtonsoft.Json.JsonConvert.SerializeObject(dolg);
            var client = new RestClient(ConfigurationManager.AppSettings.Get("apiEndpoint")+"apiDolg");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Connection", "keep-alive");
            request.AddHeader("accept-encoding", "gzip, deflate");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Accept", "*/*");
            request.AddHeader("Authorization", ConfigurationManager.AppSettings.Get("apiKey"));
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("undefined", output, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            //request.AddParameter("undefined", output, ParameterType.RequestBody);
            this.Close();
            MainWindow main = (MainWindow)Application.Current.MainWindow;
            main.UpdateProjectSelect();     
        }

        private void CloseAddDolg_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
