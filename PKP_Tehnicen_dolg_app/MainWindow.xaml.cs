﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Configuration;

namespace PKP_Tehnicen_dolg_app
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            GridDolg.Visibility = Visibility.Hidden;
            getUserPorjects(callGetApi("apiProjekt/email/", ConfigurationManager.AppSettings.Get("email")));
        }

        private void getUserPorjects(string json)
        {
            JArray projekti = JArray.Parse(json);
            foreach (JObject item in projekti)
            {
                if ((string)item["naziv"] != null)
                {
                    ComboBoxItem cbItem = new ComboBoxItem();
                    cbItem.Text = (string)item["naziv"];
                    cbItem.Value = (string)item["_id"];
                    ProjectSelect.Items.Add(cbItem);
                }
            }
        }

        private void PorjectSelect_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = ProjectSelect.SelectedItem;
            FillListBox(callGetApi("apiDolg"), ((PKP_Tehnicen_dolg_app.ComboBoxItem)item).Value.ToString());
        }

        private void FillListBox (string json, string id)
        {
            DolgoviListBox.Items.Clear();
            JArray projekti = JArray.Parse(json);
            foreach (JObject item in projekti)
            {
                if ((string)item["naziv"] != null && (string)item["id_projekt"] == id)
                {
                    ListBoxItem LsItem = new ListBoxItem();
                    LsItem.Text = (string)item["naziv"];
                    LsItem.Value = (string)item["_id"];
                    DolgoviListBox.Items.Add(LsItem);
                }
            }
        }

        private void DolgoviListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = DolgoviListBox.SelectedItem;
            if( item != null)
            {
                FillDolgBoxes(callGetApi("apiDolg/", ((PKP_Tehnicen_dolg_app.ListBoxItem)item).Value.ToString()));

            } else
            {
                GridDolg.Visibility = Visibility.Hidden;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var item = ProjectSelect.SelectedItem;
            if (item != null)
            {
                AddDolg DodajDolg = new AddDolg(((PKP_Tehnicen_dolg_app.ComboBoxItem)item).Value.ToString());
                if (((PKP_Tehnicen_dolg_app.ComboBoxItem)item).Value.ToString() != null)
                {
                    DodajDolg.Show();
                }
            }
            //this.Close();
        }

        public void UpdateProjectSelect()
        {
            var item = ProjectSelect.SelectedItem;
            FillListBox(callGetApi("apiDolg"), ((PKP_Tehnicen_dolg_app.ComboBoxItem)item).Value.ToString());

        }

        public void FillDolgBoxes(string json)
        {
            // Clear stuff
            OpisRichBox.Document.Blocks.Clear();
            GridDolg.Visibility = Visibility.Visible;

            JObject jObject = JObject.Parse(json);
            NazivTxtBox.Text = (string)jObject["naziv"];
            OpisRichBox.AppendText((string)jObject["opis"]);
            ObsegTxtBog.Text = (string)jObject["obseg"];
            KriticnostTextBox.Text = (string)jObject["kriticnost"];
        }

        public string callGetApi(string api, string value = "")
        {
            var client = new RestClient(ConfigurationManager.AppSettings.Get("apiEndpoint")+api+value);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Connection", "keep-alive");
            request.AddHeader("accept-encoding", "gzip, deflate");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Accept", "*/*");
            request.AddHeader("Authorization", ConfigurationManager.AppSettings.Get("apiKey"));
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
    }
}
