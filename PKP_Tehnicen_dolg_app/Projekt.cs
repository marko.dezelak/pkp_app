﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PKP_Tehnicen_dolg_app
{
    class Projekt
    {

        /*    JObject jObject = JObject.Parse(projekti);
            JToken jUser = jObject["user"];
            naziv = (string)jUser["naziv"];
            clan = (string)jUser["clan"];
            clan1 = (string)jUser["clan1"];
            clan2 = (string)jUser["clan2"];
            clan3 = (string)jUser["clan3"];
            clan4 = (string)jUser["clan4"];
            clan5 = (string)jUser["clan5"];
            clan6 = (string)jUser["clan6"];
            clan7 = (string)jUser["clan7"];
            clan8 = (string)jUser["clan8"];
            clan9 = (string)jUser["clan9"];
            tip_ocena = (string)jUser["tip_ocena"];
            zgornja_meja_dolga = (string)jUser["zgornja_meja_dolga"];
            datum = (DateTime)jUser["datum"];
            error = (string)jUser["error"];
        */


        public string id { get; set; }
        public string naziv { get; set; }
        public string clan { get; set; }
        public string clan1 { get; set; }
        public string clan2 { get; set; }
        public string clan3 { get; set; }
        public string clan4 { get; set; }
        public string clan5 { get; set; }
        public string clan6 { get; set; }
        public string clan7 { get; set; }
        public string clan8 { get; set; }
        public string clan9 { get; set; }
        public string tip_ocena { get; set; }
        public string zgornja_meja_dolga { get; set; }
        public string datum { get; set; }
        public string error { get; set; }
    }
}
